import React from 'react'
import DropdownUser from '@/Components/app/DropdownUser'

export default function Header({ user }) {
  return (
    <header className="sticky top-0 z-999 flex w-full bg-white drop-shadow-1">
      <div className="flex flex-grow items-center justify-end py-4 px-4 shadow-2 md:px-6 2xl:px-11">
        <div className="flex items-center gap-3 2xsm:gap-7">
          {/* <!-- User Area --> */}
          <DropdownUser user={user} />
          {/* <!-- User Area --> */}
        </div>
      </div>
    </header>
  )
}
