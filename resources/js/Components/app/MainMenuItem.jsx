import { useEffect, useRef, useState } from 'react'
import { Link } from '@inertiajs/react'
import UserOne from '../../../images/user-05.png'

export default function MainMenuItem({ user }) {
  const isActive = route().current(link + '*')

  const iconClasses = classNames('w-4 h-4 mr-2', {
    'text-white fill-current': isActive,
    'text-indigo-400 group-hover:text-white fill-current': !isActive
  })

  const textClasses = classNames({
    'text-white': isActive,
    'text-indigo-200 group-hover:text-white': !isActive
  })
  return <p>hola</p>
}
