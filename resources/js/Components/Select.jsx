import React from 'react'

export default function Select({ id, name, onChange, className, options, placeholder }) {
  //   'descripcion' in option.descripcion ? option.descripcion : option.periodicidad;
  return (
    <select className={className} name={name} id={id} onChange={onChange}>
      <option>{placeholder}</option>
      {options.map((option) => (
        <option key={option.key} value={option.key}>
          {option.value}
        </option>
      ))}
    </select>
  )
}
