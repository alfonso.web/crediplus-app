export default function InputError({ message, className = '', ...props }) {
  return message ? (
    <p {...props} className={'text-sm text-[#F87171] ' + className}>
      {message}
    </p>
  ) : null;
}
