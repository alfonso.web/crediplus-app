import { useState } from 'react'
import InputError from '@/Components/InputError'
import InputLabel from '@/Components/InputLabel'
import PrimaryButton from '@/Components/PrimaryButton'
import Select from '@/Components/Select'
import TextInput from '@/Components/TextInput'
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout'
import { Head, usePage, useForm } from '@inertiajs/react'
import { NumericFormat } from 'react-number-format'
import Breadcrumb from '@/Components/Breadcrumb'
export default function Create({ auth }) {
  const [enabled, setEnabled] = useState(false)
  const { monedas, regimen_fiscal, tipo_credito, periodicidad } = usePage().props

  const { data, setData, post, processing, errors } = useForm({
    descripcion: '',
    moneda_id: '',
    regimen_fiscal_id: '',
    tipo_credito_id: '',
    tipo_credito: '',
    periodicidad_id: '',
    periodos_gracia: false,
    maximo_periodos_gracia: '',
    capital_minimo: '',
    capital_maximo: '',
    plazo: ''
  })

  const submit = (e) => {
    e.preventDefault()
    post(route('productos-financieros.store'))
  }
  const handleRegimenFiscal = (event) => {
    const { name, value } = event.target

    const tipoCredito = tipo_credito.find((item) => {
      return item.id == value
    })

    setData((data) => ({ ...data, [name]: value }))
    setData((data) => ({
      ...data,
      ['tipo_credito_id']: tipoCredito != undefined ? tipoCredito.id : ''
    }))
    setData((data) => ({
      ...data,
      ['tipo_credito']: tipoCredito != undefined ? tipoCredito.descripcion : ''
    }))
  }

  return (
    <AuthenticatedLayout user={auth.user}>
      <Head title="Crear producto financiero" />
      <Breadcrumb pageName="Registrar producto financiero" />
      <div className="mx-auto max-w-screen-2xl p-4 md:p-6 2xl:p-10">
        <div className="mx-auto w-3/4">
          <div className="mb-6 flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between">
            {/* <h2 className="text-title-md2 font-semibold">Registrar producto financiero</h2> */}
          </div>
          <div className="grid grid-cols-5 gap-8">
            <div className="col-span-5">
              <div className="rounded-sm border border-stroke bg-white shadow-default">
                <div className="p-7">
                  <form onSubmit={submit}>
                    <div className="mb-5.5 flex flex-col gap-5.5">
                      <div className="w-full">
                        <InputLabel
                          htmlFor="descripcion"
                          value="Descripcion"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <TextInput
                            id="descripcion"
                            name="descripcion"
                            value={data.descripcion}
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            autoComplete="descripcion"
                            isFocused={true}
                            onChange={(e) => setData('descripcion', e.target.value)}
                          />
                          <InputError message={errors.descripcion} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="moneda_id"
                          value="Moneda"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="moneda_id"
                            defaultValue={data.moneda_id}
                            onChange={(e) => setData('moneda_id', e.target.value)}
                            options={monedas.map((moneda) => ({
                              key: moneda.id,
                              value: moneda.descripcion
                            }))}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona una moneda`}
                            name={`moneda_id`}
                          />
                          <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span>

                          <InputError message={errors.moneda_id} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="regimen_fiscal_id"
                          value="Régimen fiscal"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="regimen_fiscal_id"
                            defaultValue={data.regimen_fiscal_id}
                            onChange={(event) => handleRegimenFiscal(event)}
                            options={regimen_fiscal.map((regimen) => ({
                              key: regimen.id,
                              value: regimen.descripcion
                            }))}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-12 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona el régimen`}
                            name="regimen_fiscal_id"
                          />
                          <InputError message={errors.regimen_fiscal_id} className="mt-2" />
                          <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span>
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="tipo_credito"
                          value="Tipo de crédito"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <TextInput
                            id="tipo_credito"
                            name="tipo_credito"
                            value={data.tipo_credito}
                            className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter"
                            autoComplete="tipo_credito"
                            isFocused={true}
                            disabled
                            // onChange={(e) => setData('tipo_credito', e.target.value)}
                          />
                          <InputError message={errors.tipo_credito} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="tipo_tasa"
                          value="Tipo de tasa"
                          className="mb-2.5 block text-black"
                        />

                        <div className="flex flex-col gap-5.5 p-6.5">
                          <label
                            htmlFor="tasa-soluta"
                            className="flex cursor-pointer select-none items-center"
                            // className="w-full py-4 ml-2 text-sm font-medium text-gray-900"
                          >
                            <input
                              id="tasa-soluta"
                              type="radio"
                              name={`tipo_tasa`}
                              defaultValue="soluta"
                              onChange={(e) => setData('tipo_tasa', e.target.value)}
                              className="w-5 h-5 text-blue-600 mr-4 bg-gray-100 border-gray-300 focus:ring-blue-500"
                            />
                            Tasa soluta
                          </label>
                          <label
                            htmlFor="tasa-insoluta"
                            className="flex cursor-pointer select-none items-center"
                            // className="w-full py-4 ml-2 text-sm font-medium text-gray-900"
                          >
                            <input
                              id="tasa-insoluta"
                              type="radio"
                              name={`tipo_tasa`}
                              defaultValue="insoluta"
                              defaultChecked={false}
                              onChange={(e) => setData('tipo_tasa', e.target.value)}
                              className="w-5 h-5 text-blue-600 mr-4 bg-gray-100 border-gray-300 focus:ring-blue-500"
                            />
                            Tasa insoluta
                          </label>
                        </div>
                        <InputError message={errors.tipo_tasa} className="mt-2" />
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="periodicidad_id"
                          value="Periodicidad"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="periodicidad_id"
                            defaultValue={data.periodicidad_id}
                            onChange={(e) => setData('periodicidad_id', e.target.value)}
                            options={periodicidad.map((periodo) => ({
                              key: periodo.id,
                              value: periodo.periodicidad_declinacion
                            }))}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-12 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona periodicidad`}
                            name={`periodicidad_id`}
                          />
                          <InputError message={errors.periodicidad_id} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="plazo"
                          value="Plazo"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <NumericFormat
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            value={data.plazo}
                            id="plazo"
                            name="plazo"
                            onValueChange={(values) => {
                              // console.log(values.value)
                              setData('plazo', values.value)
                            }}
                          />
                          <InputError message={errors.plazo} className="mt-2" />
                        </div>
                      </div>
                      <div className="border-b border-stroke py-4">
                        <h3 className="font-medium text-black">Periodos con gracia</h3>
                      </div>
                      <div className="w-full">
                        <label
                          htmlFor="periodos_gracia"
                          className="flex cursor-pointer select-none items-center"
                        >
                          <div className="relative mr-4">
                            <input
                              type="checkbox"
                              id="periodos_gracia"
                              className="sr-only"
                              // defaultChecked={false}
                              // onChange={(e) => setData('periodos_gracia', e.target.checked)}
                              onChange={(e) => {
                                setEnabled(!enabled)
                                setData('periodos_gracia', !enabled)
                              }}
                            />
                            <div className="block h-8 w-14 rounded-full bg-meta-9"></div>
                            <div
                              className={`dot absolute left-1 top-1 flex h-6 w-6 items-center justify-center rounded-full bg-white transition ${
                                enabled && '!right-1 !translate-x-full !bg-primary'
                              }`}
                            >
                              <span className={`hidden ${enabled && '!block'}`}>
                                <svg
                                  className="fill-white"
                                  width="11"
                                  height="8"
                                  viewBox="0 0 11 8"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    d="M10.0915 0.951972L10.0867 0.946075L10.0813 0.940568C9.90076 0.753564 9.61034 0.753146 9.42927 0.939309L4.16201 6.22962L1.58507 3.63469C1.40401 3.44841 1.11351 3.44879 0.932892 3.63584C0.755703 3.81933 0.755703 4.10875 0.932892 4.29224L0.932878 4.29225L0.934851 4.29424L3.58046 6.95832C3.73676 7.11955 3.94983 7.2 4.1473 7.2C4.36196 7.2 4.55963 7.11773 4.71406 6.9584L10.0468 1.60234C10.2436 1.4199 10.2421 1.1339 10.0915 0.951972ZM4.2327 6.30081L4.2317 6.2998C4.23206 6.30015 4.23237 6.30049 4.23269 6.30082L4.2327 6.30081Z"
                                    fill=""
                                    stroke=""
                                    strokeWidth="0.4"
                                  ></path>
                                </svg>
                              </span>
                              <span className={`${enabled && 'hidden'}`}>
                                <svg
                                  className="h-4 w-4 stroke-current"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth="2"
                                    d="M6 18L18 6M6 6l12 12"
                                  ></path>
                                </svg>
                              </span>
                            </div>
                          </div>
                          Aplica periodos de gracia
                        </label>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="maximo_periodos_gracia"
                          value="Máximo de periodos permitidos"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <NumericFormat
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            value={data.maximo_periodos_gracia}
                            id="maximo_periodos_gracia"
                            name="maximo_periodos_gracia"
                            onValueChange={(values) => {
                              // console.log(values.value)
                              setData('maximo_periodos_gracia', values.value)
                            }}
                          />
                          <InputError message={errors.maximo_periodos_gracia} className="mt-2" />
                        </div>
                      </div>
                      <div className="border-b border-stroke py-4">
                        <h3 className="font-medium text-black">Límite de montos</h3>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="capital_minimo"
                          value="Capital mínimo permitido"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <NumericFormat
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            decimalScale={2}
                            value={data.capital_minimo}
                            thousandSeparator=","
                            id="capital_minimo"
                            name="capital_minimo"
                            prefix="$"
                            onValueChange={(values) => {
                              setData('capital_minimo', values.value)
                            }}
                          />
                          <InputError message={errors.capital_minimo} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="capital_maximo"
                          value="Capital máximo permitido"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <NumericFormat
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            decimalScale={2}
                            value={data.capital_maximo}
                            thousandSeparator=","
                            id="capital_maximo"
                            name="capital_maximo"
                            prefix="$"
                            onValueChange={(values) => {
                              setData('capital_maximo', values.value)
                            }}
                          />
                          <InputError message={errors.capital_maximo} className="mt-2" />
                        </div>
                      </div>
                    </div>
                    <div className="flex justify-end gap-4.5">
                      <button
                        className="flex justify-center rounded border border-stroke py-2 px-6 font-medium text-black hover:shadow-1 dark:border-strokedark dark:text-white"
                        type="submit"
                      >
                        Cancel
                      </button>
                      <PrimaryButton
                        className="flex justify-center rounded bg-primary py-2 px-6 font-medium text-gray hover:shadow-1"
                        disabled={processing}
                      >
                        Guardar
                      </PrimaryButton>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  )
}
