import Breadcrumb from '@/Components/Breadcrumb'
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout'
import { Head, usePage } from '@inertiajs/react'

export default function Index({ auth }) {
  const { productos_financieros: productosFinancieros } = usePage().props

  const { data } = productosFinancieros

  return (
    <AuthenticatedLayout user={auth.user}>
      <Head title="Productos Financieros" />
      <Breadcrumb pageName="Productos Financieros" />
      <div className="flex flex-col gap-10">
        <div className="rounded-sm border border-stroke bg-white px-5 pt-6 pb-2.5 shadow-default sm:px-7.5 xl:pb-1">
          {/* <div className="flex flex-col px-4 py-3 space-y-3 lg:flex-row lg:items-center lg:justify-end lg:space-y-0 lg:space-x-4">
          <div className="flex flex-col flex-shrink-0 space-y-3 md:flex-row md:items-center lg:justify-end md:space-y-0 md:space-x-3">
            <Link
              href={route('productos-financieros.create')}
              className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 focus:outline-none "
            >
              Nuevo producto financiero
            </Link>
          </div>
        </div> */}
          <div className="max-w-full overflow-x-auto">
            <table className="w-full table-auto">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                <tr className="bg-gray-2 text-left">
                  <th className="py-4 px-4 font-medium text-black">Descripción</th>
                  <th className="px-4 py-4">Periodicidad</th>
                  <th className="py-4 px-4 font-medium text-black">Moneda</th>
                  <th className="py-4 px-4 font-medium text-black">Regimen fiscal</th>
                  <th className="py-4 px-4 font-medium text-black">Tipo de crédito</th>
                  <th className="py-4 px-4 font-medium text-black">Tipo de tasa</th>
                  <th className="py-4 px-4 font-medium text-black text-center">
                    Periodos de gracia
                  </th>
                  <th className="py-4 px-4 font-medium text-black text-right">Capital minimo</th>
                  <th className="py-4 px-4 font-medium text-black text-right">Capital maximo</th>
                </tr>
              </thead>
              <tbody>
                {data.map(
                  ({
                    id,
                    descripcion,
                    periodicidad,
                    capital_minimo,
                    moneda,
                    regimen_fiscal,
                    tipo_credito,
                    tipo_tasa,
                    periodos_gracia,
                    capital_maximo
                  }) => {
                    return (
                      <tr key={id} className="bg-white border-b">
                        <td className="border-b border-[#eee] py-5 px-4">{descripcion}</td>
                        <td className="border-b border-[#eee] py-5 px-4">
                          {periodicidad.periodicidad_declinacion}
                        </td>
                        <td className="border-b border-[#eee] py-5 px-4">{moneda.abreviatura}</td>
                        <td className="border-b border-[#eee] py-5 px-4">
                          {regimen_fiscal.descripcion}
                        </td>
                        <td className="border-b border-[#eee] py-5 px-4">
                          {tipo_credito.descripcion}
                        </td>
                        <td className="border-b border-[#eee] py-5 px-4">{tipo_tasa}</td>
                        <td className="border-b border-[#eee] py-5 px-4 text-center">
                          {periodos_gracia ? (
                            <span className="absolute z-1 h-2 w-2 rounded-full bg-success"></span>
                          ) : (
                            <span className="absolute z-1 h-2 w-2 rounded-full bg-meta-1"></span>
                          )}
                        </td>
                        <td className="border-b border-[#eee] py-5 px-4 text-right">
                          {capital_minimo}
                        </td>
                        <td className="border-b border-[#eee] py-5 px-4 text-right">
                          {capital_maximo}
                        </td>
                      </tr>
                    )
                  }
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  )
}
