import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout'
import { Head, usePage } from '@inertiajs/react'

export default function Index({ auth }) {
  const { users } = usePage().props

  return (
    <AuthenticatedLayout user={auth.user}>
      <Head title="Users" />
      <div className="mb-6 flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between">
        <h2 className="text-title-md2 font-semibold">Usuarios registrados</h2>
      </div>
      <div className="rounded-sm border border-stroke bg-white px-5 pt-6 pb-2.5 shadow-default sm:px-7.5 xl:pb-1">
        <div className="max-w-full overflow-x-auto">
          <table className="w-full table-auto">
            <thead>
              <tr className="bg-gray-2 text-left">
                <th className="py-4 font-medium text-black xl:pl-11">Nombre</th>
                <th className="py-4 font-medium text-black">Primer apellido</th>
                <th className="py-4 font-medium text-black">Segundo apellido</th>
                <th className="py-4 font-medium text-black">Correo electronico</th>
                <th className="py-4 font-medium text-black">Fecha de creacion</th>
                <th className="py-4 font-medium text-black">Estatus</th>
                <th className="py-4 font-medium text-black">Actions</th>
              </tr>
            </thead>
            <tbody>
              {users.map(({ id, name, first_name, last_name, email, created_at, deleted_at }) => {
                return (
                  <tr key={id} className="bg-white border-b">
                    <td className="border-b border-[#eee] py-5 pl-9 xl:pl-11">{name}</td>
                    <td className="border-b border-[#eee] py-5">{first_name}</td>
                    <td className="border-b border-[#eee] py-5">{last_name}</td>
                    <td className="border-b border-[#eee] py-5">{email}</td>
                    <td className="border-b border-[#eee] py-5">{created_at}</td>
                    <td className="border-b border-[#eee] py-5">
                      <span className="absolute z-1 h-2 w-2 rounded-full bg-meta-1">
                        <span className="absolute -z-1 inline-flex h-full w-full animate-ping rounded-full bg-meta-1 opacity-75"></span>
                      </span>
                      {deleted_at ? (
                        <span className="absolute z-1 h-2 w-2 rounded-full bg-meta-1">
                          <span className="absolute -z-1 inline-flex h-full w-full animate-ping rounded-full bg-meta-1 opacity-75"></span>
                        </span>
                      ) : (
                        <span className="absolute z-1 h-2 w-2 rounded-full bg-success">
                          <span className="absolute -z-1 inline-flex h-full w-full animate-ping rounded-full bg-success opacity-75"></span>
                        </span>
                      )}
                    </td>
                    <td className="border-b border-[#eee] py-5 px-4">
                      <div className="flex items-center space-x-3.5">
                        <button className="hover:text-primary">
                          <svg
                            className="w-[16px] h-[16px] text-gray-800"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor"
                            viewBox="0 0 20 18"
                          >
                            <path d="M12.687 14.408a3.01 3.01 0 0 1-1.533.821l-3.566.713a3 3 0 0 1-3.53-3.53l.713-3.566a3.01 3.01 0 0 1 .821-1.533L10.905 2H2.167A2.169 2.169 0 0 0 0 4.167v11.666A2.169 2.169 0 0 0 2.167 18h11.666A2.169 2.169 0 0 0 16 15.833V11.1l-3.313 3.308Zm5.53-9.065.546-.546a2.518 2.518 0 0 0 0-3.56 2.576 2.576 0 0 0-3.559 0l-.547.547 3.56 3.56Z" />
                            <path d="M13.243 3.2 7.359 9.081a.5.5 0 0 0-.136.256L6.51 12.9a.5.5 0 0 0 .59.59l3.566-.713a.5.5 0 0 0 .255-.136L16.8 6.757 13.243 3.2Z" />
                          </svg>
                        </button>
                      </div>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    </AuthenticatedLayout>
  )
}
