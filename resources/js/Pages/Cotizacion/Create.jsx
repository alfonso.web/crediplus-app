import { useState } from 'react'
import InputError from '@/Components/InputError'
import InputLabel from '@/Components/InputLabel'
import PrimaryButton from '@/Components/PrimaryButton'
import Select from '@/Components/Select'
import TextInput from '@/Components/TextInput'
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout'
import { Head, usePage, useForm } from '@inertiajs/react'
import { NumericFormat } from 'react-number-format'
import Breadcrumb from '@/Components/Breadcrumb'
export default function Create({ auth }) {
  const [enabled, setEnabled] = useState(false)
  const [plazos, setPlazos] = useState([])

  const { sucursal, prospecto, tipo_credito, productos_financieros, regimen_fiscal, periodicidad } =
    usePage().props

  const { data, setData, post, processing, errors } = useForm({
    sucursal: sucursal,
    prospecto: prospecto,
    tipo_credito: tipo_credito,
    producto_financiero_id: '',
    monto: '',
    tasa_anualizada: '',
    plazo: ''
  })

  const handleProductoFinanciero = (event) => {
    const { name, value } = event.target

    const plazoProductoFinanciero = productos_financieros.find((item) => {
      return item.id == value
    })
    setData((data) => ({ ...data, [name]: value }))
    // setData((data) => ({
    //   ...data,
    //   ['plazo']: plazoProductoFinanciero != undefined ? plazoProductoFinanciero.plazo : ''
    // }))

    console.log(plazoProductoFinanciero.plazo)
  }

  const submit = (e) => {
    e.preventDefault()
    post(route('productos-financieros.store'))
  }

  return (
    <AuthenticatedLayout user={auth.user}>
      <Head title="Crear cotización" />
      <Breadcrumb pageName="Registrar cotización" />
      <div className="mx-auto max-w-screen-2xl p-4 md:p-6 2xl:p-10">
        <div className="mx-auto w-3/4">
          <div className="mb-6 flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between">
            {/* <h2 className="text-title-md2 font-semibold">Registrar producto financiero</h2> */}
          </div>
          <div className="grid grid-cols-5 gap-8">
            <div className="col-span-5">
              <div className="rounded-sm border border-stroke bg-white shadow-default">
                <div className="p-7">
                  <form onSubmit={submit}>
                    <div className="mb-5.5 flex flex-col gap-5.5">
                      <div className="w-full">
                        <InputLabel
                          htmlFor="prospecto"
                          value="Prospecto"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <TextInput
                            id="prospecto"
                            name="prospecto"
                            value={data.prospecto}
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            autoComplete="prospecto"
                            isFocused={true}
                            onChange={(e) => setData('prospecto', e.target.value)}
                          />
                          <InputError message={errors.prospecto} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="sucursal"
                          value="Sucursal"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <TextInput
                            id="sucursal"
                            name="sucursal"
                            value={data.sucursal}
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            autoComplete="prospecto"
                            isFocused={true}
                            onChange={(e) => setData('sucursal', e.target.value)}
                          />
                          <InputError message={errors.sucursal} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="tipo_credito_id"
                          value="Tipo de crédito"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="tipo_credito_id"
                            defaultValue={data.tipo_credito}
                            onChange={(e) => setData('tipo_credito', e.target.value)}
                            options={[]}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona tipo de crédito`}
                            name={`tipo_credito`}
                          />
                          <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span>

                          <InputError message={errors.tipo_credito_id} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="producto_financiero_id"
                          value="Producto financiero"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="producto_financiero_id"
                            defaultValue={data.producto_financiero_id}
                            // onChange={(e) => setData('producto_financiero_id', e.target.value)}
                            onChange={(event) => handleProductoFinanciero(event)}
                            options={productos_financieros.map((periodo) => ({
                              key: periodo.id,
                              value: periodo.descripcion
                            }))}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona el producto financiero`}
                            name={`producto_financiero_id`}
                          />
                          <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span>

                          <InputError message={errors.producto_financiero_id} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="monto"
                          value="Monto"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <NumericFormat
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            decimalScale={2}
                            value={data.monto}
                            thousandSeparator=","
                            id="monto"
                            name="monto"
                            prefix="$"
                            onValueChange={(values) => {
                              setData('monto', values.value)
                            }}
                          />
                          <InputError message={errors.monto} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="tasa_anualizada"
                          value="Tasa anualizada"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <NumericFormat
                            className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default"
                            decimalScale={2}
                            value={data.tasa_anualizada}
                            // thousandSeparator=","
                            id="tasa_anualizada"
                            name="tasa_anualizada"
                            // prefix="%"
                            onValueChange={(values) => {
                              setData('tasa_anualizada', values.value)
                            }}
                          />
                          <InputError message={errors.tasa_anualizada} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="plazo"
                          value="Plazo"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="plazo"
                            defaultValue={data.plazo}
                            onChange={(e) => setData('plazo', e.target.value)}
                            options={[]}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona el plazo`}
                            name={`plazo`}
                          />
                          <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span>

                          <InputError message={errors.plazo} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="fecha_inicio"
                          value="Fecha de inicio"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <input
                            type="date"
                            className="custom-input-date custom-input-date-1 w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                          />
                          {/* <Select
                            id="plazo"
                            defaultValue={data.plazo}
                            onChange={(e) => setData('plazo', e.target.value)}
                            options={[]}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona el plazo`}
                            name={`plazo`}
                          /> */}
                          {/* <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span> */}

                          <InputError message={errors.plazo} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="fecha_primer_vencimiento"
                          value="Fecha de primer vencimiento"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <input
                            type="date"
                            className="custom-input-date custom-input-date-1 w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                          />
                          {/* <Select
                            id="plazo"
                            defaultValue={data.plazo}
                            onChange={(e) => setData('plazo', e.target.value)}
                            options={[]}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona el plazo`}
                            name={`plazo`}
                          /> */}
                          {/* <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span> */}

                          <InputError message={errors.plazo} className="mt-2" />
                        </div>
                      </div>
                      <div className="w-full">
                        <InputLabel
                          htmlFor="numero_periodos_gracia"
                          value="Número de periodos con gracia"
                          className="mb-2.5 block text-black"
                        />

                        <div className="relative">
                          <Select
                            id="numero_periodos_gracia"
                            defaultValue={data.numero_periodos_gracia}
                            onChange={(e) => setData('numero_periodos_gracia', e.target.value)}
                            options={[]}
                            className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary"
                            placeholder={`Selecciona el numero de periodos de gracia`}
                            name={`numero_periodos_gracia`}
                          />
                          <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <g opacity="0.8">
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                                  fill="#637381"
                                ></path>
                              </g>
                            </svg>
                          </span>

                          <InputError message={errors.numero_periodos_gracia} className="mt-2" />
                        </div>
                      </div>
                    </div>
                    <div className="flex justify-end gap-4.5">
                      <button
                        className="flex justify-center rounded border border-stroke py-2 px-6 font-medium text-black hover:shadow-1 dark:border-strokedark dark:text-white"
                        type="submit"
                      >
                        Cancel
                      </button>
                      <PrimaryButton
                        className="flex justify-center rounded bg-primary py-2 px-6 font-medium text-gray hover:shadow-1"
                        disabled={processing}
                      >
                        Guardar
                      </PrimaryButton>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  )
}
