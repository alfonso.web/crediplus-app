<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Moneda;
use App\Models\TipoCredito;
use App\Models\Periodicidad;
use App\Models\RegimenFiscal;
use Illuminate\Database\Seeder;
use App\Models\ProductoFinanciero;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        RegimenFiscal::create([
            'descripcion' => 'Persona Física',
        ]);

        TipoCredito::create([
            'descripcion' => 'PF'
        ]);

        Moneda::create([
            'descripcion' => 'Peso mexicano',
            'abreviatura' => 'MXN',
            'pais' => 'México'
        ]);
        Periodicidad::create([
            'periodicidad' => 'Quincenas',
            'periodicidad_singular' => 'Quincena',
            'periodicidad_declinacion' => 'Quincenal',
            'dias' => '15'

        ]);

        Periodicidad::create([
            'periodicidad' => 'Meses',
            'periodicidad_singular' => 'Mes',
            'periodicidad_declinacion' => 'Mensual',
            'dias' => '30'

        ]);

        Periodicidad::create([
            'periodicidad' => 'Bimestres',
            'periodicidad_singular' => 'Bimestre',
            'periodicidad_declinacion' => 'Bimestral',
            'dias' => '60'
        ]);
        ProductoFinanciero::factory(12)->count(3)->create();
    }
}
