<?php

namespace Database\Factories;

use App\Models\Moneda;
use App\Models\Periodicidad;
use App\Models\ProductoFinanciero;
use App\Models\RegimenFiscal;
use App\Models\TipoCredito;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductoFinanciero>
 */
class ProductoFinancieroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'descripcion' => fake()->sentence($nbWords = 3, $variableNbWords = true),
            'periodicidad_id' => rand(1, Periodicidad::count()),
            'moneda_id' => rand(1, Moneda::count()),
            'regimen_fiscal_id' => rand(1, RegimenFiscal::count()),
            'tipo_credito_id' => rand(1, TipoCredito::count()),
            'tipo_tasa' => 'soluta',
            'periodos_gracia' => fake()->boolean,
            'maximo_periodos_gracia' => fake()->randomDigit,
            'capital_minimo' => fake()->randomFloat($nbMaxDecimals = null, $min = 100000, $max = 150000),
            'capital_maximo' => fake()->randomFloat($nbMaxDecimals = null, $min = 200000, $max = 250000),
            'plazo' => fake()->numberBetween($min = 4, $max = 48)
        ];
    }
}
