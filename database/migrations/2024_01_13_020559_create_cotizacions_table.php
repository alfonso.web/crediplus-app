<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cotizacions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('producto_financiero_id')->constrained();
            $table->integer('plazo')->nullable();
            $table->foreignId('periodicidad_id')->constrained();
            $table->integer('numero_amortizaciones')->nullable();
            $table->double('monto_otorgado', 10, 2)->nullable();
            $table->double('comision_apertura', 10, 2)->nullable();
            $table->date('fecha_inicio');
            $table->date('fecha_primer_vencimiento');
            $table->date('fecha_ultimo_vencimiento');
            $table->double('factor_moratorio', 10, 2)->nullable();
            $table->enum('periodo_anual', ['360', '365']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cotizacions');
    }
};
