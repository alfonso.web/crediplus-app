<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plazos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('producto_financiero_id')
                ->references('id')->on('producto_financieros')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->integer('plazo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plazos');
    }
};
