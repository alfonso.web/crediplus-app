<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('producto_financieros', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion')->nullable();

            $table->foreignId('periodicidad_id')
                ->references('id')->on('periodicidads')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('plazo');

            $table->foreignId('moneda_id')
                ->references('id')->on('monedas')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('regimen_fiscal_id')
                ->references('id')->on('regimen_fiscals')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('tipo_credito_id')
                ->references('id')->on('tipo_creditos')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('tipo_tasa')->nullable();

            $table->boolean('periodos_gracia')->nullable();
            $table->integer('maximo_periodos_gracia')->nullable();
            $table->double('capital_minimo', 10, 2)->nullable();
            $table->double('capital_maximo', 10, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('producto_financieros');
    }
};
