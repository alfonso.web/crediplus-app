<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plazo extends Model
{
    use HasFactory;
    protected $fillable = [
        'producto_financiero_id',
        'plazo'
    ];
}
