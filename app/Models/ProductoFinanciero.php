<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductoFinanciero extends Model
{
    use HasFactory;

    protected $fillable = [
        'descripcion',
        'periodicidad_id',
        'moneda_id',
        'regimen_fiscal_id',
        'tipo_credito_id',
        'tipo_tasa',
        'periodos_gracia',
        'maximo_periodos_gracia',
        'capital_minimo',
        'capital_maximo',
        'plazo'
    ];

    /**
     * Obtener la periodicidad de un producto financieroa.
     */
    public function periodicidad(): BelongsTo
    {
        return $this->belongsTo(Periodicidad::class, 'periodicidad_id');
    }

    /**
     * Obtener la moneda de un producto financieroa.
     */
    public function moneda(): BelongsTo
    {
        return $this->belongsTo(Moneda::class, 'moneda_id');
    }

    /**
     * Obtener el regimen fiscal de un producto financieroa.
     */
    public function regimenFiscal(): BelongsTo
    {
        return $this->belongsTo(RegimenFiscal::class, 'regimen_fiscal_id');
    }

    /**
     * Obtener el regimen fiscal de un producto financieroa.
     */
    public function tipoCredito(): BelongsTo
    {
        return $this->belongsTo(TipoCredito::class, 'tipo_credito_id');
    }
}
