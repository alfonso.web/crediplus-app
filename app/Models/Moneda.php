<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Moneda extends Model
{
    use HasFactory;

    /**
     * Get the comments for the blog post.
     */
    public function productosFinancieros(): HasMany
    {
        return $this->hasMany(ProductoFinanciero::class);
    }
}
