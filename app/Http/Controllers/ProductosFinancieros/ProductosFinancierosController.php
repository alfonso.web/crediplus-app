<?php

namespace App\Http\Controllers\ProductosFinancieros;

use Inertia\Inertia;
use App\Models\Moneda;
use App\Models\TipoCredito;
use App\Models\Periodicidad;
use Illuminate\Http\Request;
use App\Models\RegimenFiscal;
use App\Models\ProductoFinanciero;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\StoreProductoFinancieroRequest;
use App\Http\Resources\ProductoFinancieroCollection;
use App\Models\Plazo;

class ProductosFinancierosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('ProductosFinancieros/Index', [
            'productos_financieros' => new ProductoFinancieroCollection(ProductoFinanciero::with(['periodicidad', 'moneda'])->get())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $monedas = Moneda::get();
        $regimenFiscal = RegimenFiscal::get();
        $tipoCredito = TipoCredito::get();
        $periodicidad = Periodicidad::get();

        return Inertia::render('ProductosFinancieros/Create', [
            'monedas' => $monedas,
            'regimen_fiscal' => $regimenFiscal,
            'tipo_credito' => $tipoCredito,
            'periodicidad' => $periodicidad
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductoFinancieroRequest $request)
    {
        $productoFinanceroCreado = ProductoFinanciero::create(
            $request->validated()
        );
        // dd($productoFinanceroCreado);
        collect(range(1, $request->plazo))->each(function ($plazo) use ($productoFinanceroCreado) {
            Plazo::create([
                'producto_financiero_id' => $productoFinanceroCreado->id,
                'plazo' => $plazo
            ]);
        });
        // dd(collect(range(1, $request->plazo)));
        return to_route('productos-financieros.index')->with('success', 'Producto financiero creado.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
