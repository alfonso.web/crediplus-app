<?php

namespace App\Http\Controllers\Cotizacion;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductoFinanciero;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $productosFinancieros = ProductoFinanciero::get();
        return Inertia::render('Cotizacion/Create', [

            'prospecto' => fake()->name . ' ' . fake()->firstName . ' ' . fake()->lastName,
            'sucursal' => fake()->company,
            'tipo_credito' => 'PF',
            'productos_financieros' => $productosFinancieros
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
