<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Support\Number;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductoFinancieroCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return ['data' => $this->collection->map(function ($item) {
            return [
                'id' => $item->id,
                'descripcion' => $item->descripcion,
                'periodicidad' => $item->periodicidad,
                'moneda' => $item->moneda,
                'regimen_fiscal' => $item->regimenFiscal,
                'tipo_credito' => $item->tipoCredito,
                'tipo_tasa' => $item->tipo_tasa,
                'periodos_gracia' => $item->periodos_gracia,
                'capital_minimo' => Number::currency($item->capital_minimo),
                'capital_maximo' => Number::currency($item->capital_maximo),
            ];
        })];

        // return ['data' => $this->collection->map->only(
        //     'id',
        //     'descripcion',
        //     'periodicidad',
        //     'moneda',
        //     'capital_minimo',
        //     Number::format(capital_minimo)
        // )];
    }
}
