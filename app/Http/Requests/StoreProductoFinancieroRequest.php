<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductoFinancieroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'descripcion' => 'required',
            'periodicidad_id' => ['required', 'exists:App\Models\Periodicidad,id'],
            'moneda_id' => ['required', 'exists:App\Models\Moneda,id'],
            'regimen_fiscal_id' => ['required', 'exists:App\Models\RegimenFiscal,id'],
            'tipo_credito_id' => ['required', 'exists:App\Models\TipoCredito,id'],
            'tipo_credito' => ['required'],
            'tipo_tasa' => ['required'],
            'periodos_gracia' => ['boolean'],
            'maximo_periodos_gracia' => ['required_if_accepted:periodos_gracia'],
            'capital_minimo' => ['required', 'numeric'],
            'capital_maximo' => ['required', 'numeric', 'gt:capital_minimo'],
            'plazo' => ['required', 'numeric']
        ];
    }

    /**
 * Get the error messages for the defined validation rules.
 *
 * @return array<string, string>
 */
    public function messages(): array
    {
        return [
            'descripcion.required' => 'Descripción es obligatorio.',
            'moneda_id.required' => 'Moneda es obligatorio.',
            'moneda_id.exists' => 'La identificación de moneda seleccionada no es válida.',
            'regimen_fiscal_id.required' => 'Regimen fiscal es obligatorio.',
            'regimen_fiscal_id.exists' => 'La identificación del regimen fiscal seleccionada no es válida.',
            'tipo_credito_id.required' => 'Tipo de crédito es obligatorio.',
            'tipo_credito_id.exists' => 'La identificación del crédito seleccionada no es válida.',
            'tipo_credito.required' => 'Tipo de crédito es obligatorio.',
            'tipo_tasa.required' => 'Tipo de tasa es obligatorio.',
            'periodicidad_id.required' => 'Periodicidad es obligatorio.',
            'periodicidad_id.exists' => 'La identificación de la periodicidad seleccionada no es válida.',
            'maximo_periodos_gracia.required_if_accepted' => 'El campo maximo periodos gracia es obligatorio cuando se acepta periodos gracia.',
            'capital_minimo.required' => 'Monto del capital minimo es obligatorio.',
            'capital_maximo.required' => 'Monto del capital maximo es obligatorio.',
            'capital_maximo' => 'Capital maximo debe ser mayor que capital minimo.',
            'plazo.required' => 'Plazo es obligatorio.',
        ];
    }
}
